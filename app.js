const config = require("./config");
const Cookies = require("cookies");
const express = require("express");
const app = express();//等同于http.createServer();
const User = require("./models/user");
const jwt = require("jsonwebtoken");

//内置中间件加载
app.use(express.urlencoded({extended: false}));//获取data信息并构建body

app.use(async function (req, res, next) {
    req.cookies = new Cookies(req, res);//动态添加cookie，目的是跨模块使用
    const tokenFromClient = req.cookies.get("token");
    console.log(tokenFromClient);

    if (typeof tokenFromClient !== 'undefined') {
        let token = JSON.parse(tokenFromClient);
        token = token.Authorization.replace('Bearer', '');
        jwt.verify(token, config.jwt.secret, async function (err, decodedToken) {
            if (err) {
                console.log(err);
            }
            try {
                console.log(decodedToken._id);
                console.log(token);
                const user = await User.findOne({_id: decodedToken._id, "tokens.token": token})
                if (user) {
                    req.user = user;
                    next();
                } else {
                    req.cookies.set("token", null);
                    res.send("已退出，请重新登录");
                }
            } catch (e) {
                console.log('cookies error:' + e);
            }
        })
    } else {
        next();
    }
});

//将站点路径映射到本地静态文件路径
app.use("/public", express.static(__dirname + "/public"));
app.use('/uploads/avatars', express.static(__dirname + '/uploads/avatars'));

//路由设置，目录深的写在前面以提升服务器性能
app.use('/admin', require('./routers/admin.js'));
app.use('/api', require('./routers/api.js'));
app.use('/', require('./routers/main.js'));

//模板引擎配置
const swig = require("swig");
app.set('views', './views');//指定模板(views)目录
app.set('view engine', 'html');//指定模板文件类型，与下一句文件类型对应
app.engine('html', swig.renderFile);//指定渲染方法
swig.setDefaults({cache: false});//关闭缓存，todo: 发布时删除

//数据库
const mongoose = require('mongoose');
mongoose.connect(`mongodb://localhost:${config.app.dbPort}/blog`,
    {useNewUrlParser: true, useUnifiedTopology: true},
    function (err) {
        if (err) {
            console.log(`db connect failure`);
        } else {
            console.log(`db connect successfully on port ${config.app.dbPort}`);
            app.listen(config.server.port);
            console.log(`server is running at ${config.server.host}:${config.server.port}`);
        }
    }
);