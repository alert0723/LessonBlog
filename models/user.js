/*
* https://mongoosejs.com/docs/index.html
*/

const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");

let schema = new mongoose.Schema({
    username: {
        type: String,//数据类型
        unique: true,//唯一性
        required: true//是否必须
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase: true,
        minlength: 6,
        maxlength: 30,
        validator: function (value) {
            if (!validator.isEmail(value)) {
                console.log("Email Invalid!");
            }
        }
    },
    activatedLogs: [{
        code: {
            type: String,
            require: true
        },
        date: {
            type: Date,
            default: Date.now()
        }
    }],
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isLive: {
        type: Boolean,
        default: false
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    avatarPath: {
        type: String,
        default: "uploads/avatars/default.png"
    },
    role: {
        type: String,
        default: "student"
    }
})

schema.statics.findByCredentials = async function (username, password) {
    const user = await model.findOne({username: username});
    if (!user)
        console.log("无法登录");

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch)
        console.log("无法登录");
    return user;
}

schema.methods.generateAuthToken = async function () {
    const user = this;
    const token = jwt.sign({_id: user._id.toString()}, config.jwt.secret, {expiresIn: config.jwt.expiresIn});
    user.tokens = user.tokens.concat({token: token});
    await user.save();
    return token;
}

schema.pre('save', async function (next) {
    const user = this;
    let hashed = await bcrypt.hash(user.password, 8);//加密
    user.password = hashed;

    next();
})

let model = mongoose.model('User', schema);

module.exports = model;