const mongoose = require('mongoose');

// 链接表结构
let model = mongoose.model('Asset', new mongoose.Schema({
        filename: String,
        path: String
    })
);

module.exports = model;