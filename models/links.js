const mongoose = require("mongoose");
let model = mongoose.model('Link', new mongoose.Schema({
        name: String,
        url: String
    })
);

module.exports = model;