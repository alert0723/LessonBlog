const mongoose = require("mongoose");
let model = mongoose.model('Blog', new mongoose.Schema({
        title: String,
        category: {
            type: String,
            required: true
        },
        author: {
            type: String
        },
        authorId: {
            type: String,
            required: true
        },
        addTime: {
            type: Date,
            required: true,
            default: new Date()
        },
        view: {
            type: Number,
            default: 0
        },
        content: {
            type: String,
            default: ''
        },
        enclosurePath: {
            type: String,
            default: ''
        },
        enclosureName: {
            type: String,
            default: ''
        }
    })
);

module.exports = model;