$(function () {
    let $regBox = $('#registerBox');
    let $loginBox = $('#loginBox');
    let $userInfo = $('#userInfo');

    $regBox.find("a.colMint").on('click', function () {
        $regBox.hide();
        $loginBox.show();
    });
    $loginBox.find("a.colMint").on('click', function () {
        $regBox.show();
        $loginBox.hide();
    });

    //注册register
    //事件源btnReg，事件click，事件监听器cb/事件适配器（adapter）
    $('#btnReg').on('click', function () {
        $.ajax({
            type: "post",
            url: "/api/account/register",
            data: {
                email: $regBox.find('[name="email"]').val(),
                username: $regBox.find('[name="username"]').val(),
                password: $regBox.find('[name="password"]').val(),
                passwordAgain: $regBox.find('[name="passwordAgain"]').val()
            },
            dataType: "json",
            success: function (result) {
                if (result.code === "100") {
                    $userInfo.fadeIn(1000);
                    $regBox.addClass("hidden");
                }
                window.location.reload();
                console.log(result.msg);
            }
        })
    })

    $('#btnLogin').on('click', function () {
        $.ajax({
            type: "post",
            url: "/api/account/login",
            data: {
                username: $loginBox.find('[name="username"]').val(),
                password: $loginBox.find('[name="password"]').val(),
                passwordAgain: $loginBox.find('[name="passwordAgain"]').val()
            },
            dataType: "json",
            success: function (result) {
                if (result.code === "200") {
                    // $userInfo.fadeIn(1000);
                    // $loginBox.addClass("hidden");
                    // $userInfo.find('.username').text(result.user.username);
                    window.location.reload();
                }
                console.log(result.msg);
            }
        })
    });

    $('#logoutBtn').on('click', function () {
        $.ajax({
            type: "get",
            url: "/api/account/logout",
            dataType: "json",
            success: function () {
                window.location.reload();
            }
        })
    });

});