const nodemailer = require("nodemailer");
const User=require("../models/user");

module.exports = {
    sendActMail: async function (email, code, user_id) {
        //发送方邮箱
        let transporter = nodemailer.createTransport({
            service: '163',
            port: 25,
            secure: false,// true for 465, false for other ports
            auth: {
                user: 'msyqgzt_nick@163.com', // generated ethereal user 邮箱地址
                pass: 'LFSZYLDTORBVIGHW', // generated ethereal password 授权码
            },
        });

        // 接收方邮箱
        let info = await transporter.sendMail({
            from: '"Fred Foo 👻" <msyqgzt_nick@163.com>', // sender address
            to: email, // list of receivers
            subject: "邮箱验证激活", // Subject line
            //text: '点击激活：<a href="http://localhost:8080/account/active?id='+ user_id +'&code='+ code + '"></a>',// plain text body
            html: '<a href="http://localhost:8080/account/active?id=' + user_id + '&code=' + code + '">点击激活</a>'// html body
        });
        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    },

    //发送重置密码的验证码
    sendResetEmail: async function (email, randomCode) {
        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        //let testAccount = await nodemailer.createTestAccount();
        const randomFns = () => {
            return (1000 + Math.round(Math.random() * 10000 - 1000))
        };

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
           host: "smtp.163.email",
            service: '163',
            port: 25,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'msyqgzt_nick@163.com', // generated ethereal user 邮箱地址
                pass: 'LFSZYLDTORBVIGHW', // generated ethereal password 授权码
            },
        });

        // send mail with defined transport object

        let info = await transporter.sendMail({
            from: '"Fred Foo 👻" <msyqgzt_nick@163.com>', // sender address
            to: email, // list of receivers
            subject: "Hello ✔", // Subject line
            text: '验证码：' + randomCode,// plain text body
            //html: // html body
        });

        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    },

    //发送随机的重置后的密码
    sendResetPsw: async function (email, randomCode) {

        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        //let testAccount = await nodemailer.createTestAccount();
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            //host: "smtp.163.email",
            service: '163',
            port: 25,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'msyqgzt_nick@163.com', // generated ethereal user 邮箱地址
                pass: 'LFSZYLDTORBVIGHW', // generated ethereal password 授权码
            },
        });

        // send mail with defined transport object

        let info = await transporter.sendMail({
            from: '"Fred Foo 👻" <msyqgzt_nick@163.com>', // sender address
            to: email, // list of receivers
            subject: "Hello ✔", // Subject line
            text: '新密码：' + randomCode,// plain text body
            //html: // html body
        });

        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }
};