let config = {
    server: {
        host: "localhost",
        port: 8080
    },
    app: {
        dbPort: 32023
    },
    paginationSettings: {
        "pageSize": 10
    },
    jwt: {
        'secret': 'DigitalMediaTechnology',
        'expiresIn': '7 days'
    },
    __rootDir: __dirname,
    assetDirs: {
        documents: "",
        avatar: "uploads"
    }
}

module.exports = config;