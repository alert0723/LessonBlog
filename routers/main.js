const express = require('express');
const config = require("../config");
const bcrypt = require("bcryptjs");
const emailSend = require("../tools/nodemailer");
const fs = require("fs");
const xlsx = require("xlsx");

const User = require("../models/user");
const Categories = require("../models/categories");
const Blogs = require("../models/blogs");
const Links = require("../models/links");

//express下的路由组件
const router = express.Router();

let randomCode;
let email;
let basePath = config.__rootDir + "/uploads/assets/";

router.use(async function (req, res, next) {
    console.log("Main router");
    //查找初始管理员账号
    const user = await User.findOne({username: 'admin'});
    if (!user) {
        let newUser = new User({
            email: 'administrator@163.com',
            username: 'admin',
            password: 'admin',
            isAdmin: true,
            isLive: true
        });
        await newUser.save();
    }

    //自动批量注册资源库学生账号
    const readDir = fs.readdirSync(basePath);
    for (let i = 0; i < readDir.length; i++) {
        const filePath = basePath + readDir[i];
        if (!fs.existsSync(filePath)) {
            console.log("file not found")
        }
        const workbook = xlsx.readFile(filePath);
        const sheet = workbook.Sheets.Sheet1;
        const count = sheet['!ref'].split(':O')[1];
        for (let j = 5; j < count; j++) {
            await User.findOne({
                username: JSON.stringify(sheet['A' + j].v)
            }).then(function (user) {
                if (!user) {
                    let newUser = new User({
                        username: JSON.stringify(sheet['A' + j].v),
                        password: '123456',
                        isAdmin: true,
                        isLive: true,
                        email: JSON.stringify(sheet['I' + j].v).split("\"")[1]
                    });
                    newUser.save();
                }
            })
        }
    }
    next();
});

router.get('/', async function (req, res, next) {
    let page = Number(req.query.page || 1);
    let count = await Blogs.count();
    let pageLimit = 3;//config.paginationSettings.pageSize;
    let totalPages = Math.ceil(count / pageLimit);
    let prePage = Math.max(page - 1, 1);
    let nextPage = Math.min(totalPages, page + 1);
    let skip = (page - 1) * pageLimit;

    let categories = await Categories.find();
    let category = await Categories.findOne({_id: req.query.category});

    let blogs = await Blogs.find().sort({_id: -1}).limit(pageLimit).skip(skip);
    let links = await Links.find();

    res.render("./main/index", {
        user: req.user,
        url: "/",
        links: links,
        blogs: blogs,//所有博文对象
        categories: categories,//所有分类对象
        category: category,//当前查看的分类对象
        count: count,
        page: page,
        totalPages: totalPages,
        prePage: prePage,
        nextPage: nextPage
    });
})

router.get('/view', async function (req, res, next) {
    let id = req.query.id;
    let blog = await Blogs.findOne({_id: id});
    await blog.update({view: blog.view + 1});
    blog = await Blogs.findOne({_id: id});

    let author = await User.findOne({_id: blog.authorId.toString()});

    if (!author)
        author = new User({username: "未知"});

    res.render('./main/view', {
        title: blog.title,
        author: author.username,
        view: blog.view,
        content: blog.content,
        category: blog.category,
        time: blog.addTime,
        fileName: blog.enclosureName,
        filePath: blog.enclosurePath
    });
});

//下载附件
router.get('/download', function (req, res, next) {
    let path = req.query.path;
    let road = config.__rootDir + '/' + path;
    res.download(road);
});

//邮箱激活
router.get('/account/active', function (req, res, next) {
    let id = req.query.id || '';
    let code = req.query.code;

    User.findOne({
        _id: id
    }).then(function (user) {
        if (user.activatedLogs[0].code === code && (Date.now() - user.activatedLogs[0].date) / (1000 * 3600 * 24) < 30) {
            User.update({_id: id}, {isLive: true}, function (err) {
                if (err) {
                    res.send('更新失败');
                } else {
                    res.render('email_templates/greeting', {
                        title: '登录',
                        error: '激活成功，请登录！'
                    });
                }
            });
        } else {
            res.send('失败');
        }
    })
});

//重置密码
router.get('/account/forget', function (req, res, next) {
    res.render("user/verify");
});

router.post('/account/forget', async function (req, res, next) {
    let code = req.body.code;

    function randomFns() {
        return (1000 + Math.round(Math.random() * 10000 - 1000))
    }

    function randomPsw() {
        return (100000 + Math.round(Math.random() * 1000000 - 100000))
    }

    if (typeof code !== 'undefined') {
        if (code === randomCode.toString()) {
            const newPsw = randomPsw();
            const hashedPwd = await bcrypt.hash(newPsw.toString(), 8);
            await emailSend.sendResetPsw(email, newPsw);
            User.updateOne(
                {email: email},
                {
                    password: hashedPwd,
                }).then(function () {
                res.render("user/success")
            });
        }
    } else {
        email = req.body.email;
        randomCode = randomFns();
        await emailSend.sendResetEmail(email, randomCode);
        res.render("user/reset");
    }

});

module.exports = router;