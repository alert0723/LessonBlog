const express = require('express');
const router = express.Router();
const User = require("../models/user");
const auth = require("../middleware/auth");
const bcrypt = require('bcryptjs');
const {v4: uuidv4} = require('uuid');
const emailSend = require("../tools/nodemailer");
const multer = require('multer');
let rspMsg = {};

router.use(function (req, res, next) {
    rspMsg = {
        code: 0,
        msg: ''
    };
    next();
});

router.post('/account/register', async function (req, res, next) {
    let email = req.body.email;
    let username = req.body.username;
    let password = req.body.password;
    let passwordAgain = req.body.passwordAgain;

    let activatedLogs = uuidv4();
    const user = await User.findOne({username: username});
    const useremail = await User.findOne({email: email});

    //用户密码是否为空？
    if (username === "") {
        rspMsg.code = "101";
        rspMsg.msg = "用户名为空"
        res.send(rspMsg);
    }
    if (password === "") {
        rspMsg.code = "102";
        rspMsg.msg = "密码为空"
        res.send(rspMsg);
    }

    //用户是否存在？
    if (user) {
        rspMsg.code = "103";
        rspMsg.msg = "用户已存在"
        res.send(rspMsg);
    }
    //两次输入密码是否一致？
    if (password !== passwordAgain) {
        rspMsg.code = "104";
        rspMsg.msg = "密码输入不一致";
        res.send(rspMsg);
    }
    if (email === "") {
        rspMsg.code = "105";
        rspMsg.msg = "邮箱不能为空";
        res.send(rspMsg);
    }
    if (useremail) {
        rspMsg.code = "106";
        rspMsg.msg = "邮箱已存在";
        res.send(rspMsg);
    }

    let newUser = new User({
        username: username,
        password: password,
        isAdmin: true,
        email: email,
        activatedLogs: {
            code: activatedLogs,
            date: Date.now()
        }
    });
    await newUser.save();
    await emailSend.sendActMail(
        email,
        activatedLogs,
        newUser.id
    );//发送激活邮件

    rspMsg.user = newUser;
    rspMsg.code = "100";
    rspMsg.msg = "注册成功"
    //自动登录
    req.cookies.set("__user", JSON.stringify({
        _id: newUser._id,
        username: newUser.username
    }));

    res.send(rspMsg);
});

router.post('/account/login', async function (req, res, next) {
    let username = req.body.username;
    let password = req.body.password;

    const usernameFromDb = await User.findOne({username: username});
    // const userFromDb = await User.findOne({username: username, password: hashPwd});
    const userFromDb = await User.findByCredentials(username, password);//钩子
    if (userFromDb) {
        rspMsg.user = userFromDb;
        rspMsg.code = "200";
        rspMsg.msg = "登陆成功";

        const token = await userFromDb.generateAuthToken();
        rspMsg.token = {
            Authorization: 'Bearer' + token
        }//生成用户令牌
        // res.head('Authorization');

        req.cookies.set("token", JSON.stringify(rspMsg.token));
        // {
        //     _id: userFromDb._id,
        //     username: userFromDb.username
        // }
    } else if (usernameFromDb) {
        rspMsg.code = "201";
        rspMsg.msg = "用户名不存在";
    } else {
        rspMsg.code = "202";
        rspMsg.msg = "用户名或密码错误";
    }
    res.send(rspMsg);
});

router.get('/account/logout', function (req, res, next) {
    req.cookies.set("token", null);
    res.send(rspMsg);
});

router.get('account/logoutAll', auth.hasSignedIn, async function (req, res, next) {
    //清空浏览器cookie
    req.cookies.set("token", null);
    //清空数据库中的tokens
    req.user.tokens = [];
    await req.user.save();

    res.send(rspMsg);
});

//上传头像
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/avatars/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
let avatar = multer({
    storage: storage,
    function(req, file, cb) {
        if (!file.originalname.match(/\.(jpeg|png|gif)$/)) {
            cb(null, false);
            cb(new Error('不允许的文件类型'));
        }
        cb(null, true)
    }
});
router.post('/account/avatar', avatar.single('avatar'), async (req, res, next) => {
    let username = req.user.username;
    let file = req.file;
    if (typeof file !== 'undefined') {
        let path = file.path;
        await User.updateOne({
            username: username
        }, {
            avatarPath: path
        }).then(function () {
            window.location.reload();
        });
    }
});

module.exports = router;