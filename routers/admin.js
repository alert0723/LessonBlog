const express = require('express');
const router = express.Router();
const User = require("../models/user");
const Categories = require("../models/categories");
const Blogs = require("../models/blogs");
const Links = require("../models/links");
const Asset = require("../models/asset");
const config = require("../config");

const multer = require('multer');

router.use(function (req, res, next) {
    if (!req.user.isAdmin) {
        res.send('对不起，您没有权限访问后台');
        return;
    }
    next();
});

router.get('/', function (req, res, next) {
    res.render('admin/index', {
        userInfo: req.user
    });
});

router.get('/user', async function (req, res, next) {
    let page = Number(req.query.page || 1);//将参数序列化成json
    let count = await User.count();
    let pageLimit = config.paginationSettings.pageSize;
    let totalPages = Math.ceil(count / pageLimit);
    let prePage = Math.max(page - 1, 1);
    let nextPage = Math.min(totalPages, page + 1);

    let skip = (page - 1) * pageLimit;
    let users = await User.find({}).skip(skip).limit(pageLimit);//跳过m条，获取n条
    res.render('admin/user_index',
        {
            userInfo: req.user,
            url: '/admin/user',
            users: users,
            count: count,
            page: page,
            totalPages: totalPages,
            prePage: prePage,
            nextPage: nextPage
        });
});
//另一种方式
// router.get('/user/:_page', async function (req, res, next) {
//     let page = query(req.param._page);//将参数序列化成json
//
// });

//用户数据更新页面
router.get('/user/update', async function (req, res, next) {
    let _userId = req.query.id;
    let user = await User.findById(_userId);
    res.render('admin/user_update', {user: user});
});
router.post('/user/update', async function (req, res, next) {
    let username = req.body.username;
    let password = req.body.password;
    let _userId = req.query.id;
    let user = await User.findById(_userId);
    await user.update({username: username, password: password});
    res.render('admin/user_index', {users: user});
});

//博文分类
router.get('/categories', async function (req, res, next) {
    let page = Number(req.query.page || 1);
    let count = await Categories.count();
    let pageLimit = config.paginationSettings.pageSize;
    let totalPages = Math.ceil(count / pageLimit);
    let prePage = Math.max(page - 1, 1);
    let nextPage = Math.min(totalPages, page + 1);
    let skip = (page - 1) * pageLimit;

    let categories = await Categories.find().sort({_id: -1}).limit(pageLimit).skip(skip);
    res.render('admin/categories_index', {
        userInfo: req.user,
        categories: categories,
        url: "/admin/categories",
        count: count,
        page: page,
        totalPages: totalPages,
        prePage: prePage,
        nextPage: nextPage
    });

});
//分类数据更新页面
router.get('/categories/edit', async function (req, res, next) {
    let _categoryId = req.query.id;
    let category = await Categories.findById(_categoryId);
    res.render('admin/categories_edit', {category: category});
});
router.post('/categories/edit', async function (req, res, next) {
    let id = req.query.id || ''
    let name = req.body.categoryName || ''

    // 判断数据库中是否存在同名分类
    Categories.findOne({_id: id}).then(function (category) {
        if (!category) {
            res.render('admin/error', {
                userInfo: req.userInfo,
                message: '分类信息不存在'
            })
            return Promise.reject();
        } else {
            if (name === category.name) {
                res.render('admin/success', {
                    userInfo: req.userInfo,
                    message: '修改成功',
                    url: '/admin/categories'
                })
                return Promise.reject();
            } else {
                return Categories.findOne({_id: {$ne: id}, name: name});
            }
        }
    }).then(function (sameCategory) {
        if (sameCategory) {
            res.render('admin/error', {
                userInfo: req.userInfo,
                message: '已经存在同名分类',
            })
            return Promise.reject()
        } else {
            let category = Categories.findOne({_id: id});
            return category.update({name: name});
        }
    }).then(function () {
        res.render('admin/success', {
            userInfo: req.userInfo,
            message: '修改成功',
            url: '/admin/categories'
        })
    })
});
//分类数据创建页面
router.get('/categories/create', async function (req, res, next) {
    res.render('admin/categories_create');
});
router.post('/categories/create', async function (req, res, next) {
    let name = req.body.categoryName || ''
    if (name === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '分类名称不能为空'
        })
        return;
    }
    // 判断数据库中是否存在同名分类
    Categories.findOne({name: name}).then(function (res) {
        if (res) {
            res.render('admin/error', {
                userInfo: req.userInfo,
                message: '分类已经存在了'
            })
            return Promise.reject()
        } else {
            return new Categories({
                name: name
            }).save()
        }
    }).then(function (newcategory) {
        res.render('admin/success', {
            userInfo: req.userInfo,
            message: '分类保存成功',
            url: '/admin/categories'
        })
    })
});

//分类的删除
router.get('/categories/delete', (req, res) => {
    let id = req.query.id || ''
    Categories.remove({_id: id}).then(function () {
        res.render('admin/success', {
            userInfo: req.userInfo,
            message: '删除成功',
            url: '/admin/categories'
        })
    })
});


let fileStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/enclosures/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

let enclosure = multer({storage: fileStorage});

//博文列表
router.get('/blogs', async function (req, res, next) {
    let page = Number(req.query.page || 1);
    let count = await Blogs.count();
    let pageLimit = config.paginationSettings.pageSize;
    let totalPages = Math.ceil(count / pageLimit);
    let prePage = Math.max(page - 1, 1);
    let nextPage = Math.min(totalPages, page + 1);
    let skip = (page - 1) * pageLimit;

    let categories = await Categories.find();
    let blogs = await Blogs.find().sort({_id: -1}).limit(pageLimit).skip(skip);
    res.render('admin/blogs_index', {
        userInfo: req.user,
        blogs: blogs,
        categories: categories,
        url: "/admin/blogs",
        count: count,
        page: page,
        totalPages: totalPages,
        prePage: prePage,
        nextPage: nextPage
    });
});
//博文创建页面
router.get('/blogs/create', async function (req, res, next) {
    let categories = await Categories.find();
    res.render('admin/blogs_create', {
        categories: categories
    });
});
router.post('/blogs/create', enclosure.single("enclosure"), async function (req, res, next) {
    let title = req.body.blogTitle || '';
    let content = req.body.blogContent || '';
    let select = req.body.blogCategory;
    let category = await Categories.findOne({name: select});
    category = category.name;

    if (title === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '标题不能为空'
        })
        return;
    }
    if (content === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '内容不能为空'
        });
        return;
    }
    let author = await User.findOne({_id: req.user._id});
    let blog = new Blogs({
        title: title,
        category: category,
        author: author.username,
        authorId: req.user._id.toString(),
        content: content
    });

    let file = req.file;
    if (typeof file !== 'undefined') {
        let path = file.path;
        let filename = file.filename;
        console.log("path:" + path);
        console.log("filename:" + filename);
        blog.enclosurePath = path;
        blog.enclosureName = filename;
    }

    await blog.save();

    res.render('admin/success', {
        userInfo: req.userInfo,
        message: '博文发布成功',
        url: '/admin/blogs'
    })
});
router.get('/blogs/edit', async function (req, res, next) {
    let _blogsId = req.query.id;
    let blog = await Blogs.findById(_blogsId);
    res.render('admin/blogs_edit', {blog: blog});
});
router.post('/blogs/edit', async function (req, res, next) {
    let title = req.body.blogTitle || '';
    let content = req.body.blogContent || '';
    if (title === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '标题不能为空'
        });
        return;
    }
    if (content === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '内容不能为空'
        });
        return;
    }

    let blog = Blogs.findOne({_id: req.query.id});
    await blog.update({
        title: title,
        addTime: new Date(),
        content: content
    });

    res.render('admin/success', {
        userInfo: req.userInfo,
        message: '修改成功',
        url: '/admin/blogs'
    })
});
//博文删除
router.get('/blogs/delete', (req, res) => {
    let id = req.query.id || ''
    Blogs.remove({_id: id}).then(function () {
        res.render('admin/success', {
            userInfo: req.userInfo,
            message: '删除成功',
            url: '/admin/blogs'
        })
    })
});


//友联列表
router.get('/links', async function (req, res, next) {
    let page = Number(req.query.page || 1);
    let count = await Blogs.count();
    let pageLimit = config.paginationSettings.pageSize;
    let totalPages = Math.ceil(count / pageLimit);
    let prePage = Math.max(page - 1, 1);
    let nextPage = Math.min(totalPages, page + 1);
    let skip = (page - 1) * pageLimit;

    let links = await Links.find().sort({_id: -1}).limit(pageLimit).skip(skip);
    res.render('admin/links_index', {
        userInfo: req.user,
        links: links,
        url: "/admin/links",
        count: count,
        page: page,
        totalPages: totalPages,
        prePage: prePage,
        nextPage: nextPage
    });
});
//博文创建页面
router.get('/links/create', async function (req, res, next) {
    let links = await Links.find();
    res.render('admin/links_create', {
        links: links
    });
});
router.post('/links/create', async function (req, res, next) {
    let name = req.body.linkName || '';
    let url = req.body.linkUrl || '';

    if (name === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '名称不能为空'
        })
        return;
    }
    if (url === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '链接不能为空'
        });
        return;
    }

    let link = new Links({
        name: name,
        url: url
    });
    await link.save();
    res.render('admin/success', {
        userInfo: req.userInfo,
        message: '友链发布成功',
        url: '/admin/links'
    })
});
router.get('/links/edit', async function (req, res, next) {
    let _linksId = req.query.id;
    let link = await Links.findById(_linksId);
    console.log(link);
    res.render('admin/links_edit', {link: link});
});
router.post('/links/edit', async function (req, res, next) {
    let name = req.body.linkName || '';
    let url = req.body.linkUrl || '';
    if (name === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '名称不能为空'
        })
        return;
    }
    if (url === '') {
        res.render('admin/error', {
            userInfo: req.userInfo,
            message: '链接不能为空'
        });
        return;
    }
    let link = Links.findOne({_id: req.query.id});
    await link.update({
        name: name,
        url: url
    });
    res.render('admin/success', {
        userInfo: req.userInfo,
        message: '修改成功',
        url: '/admin/links'
    });
});
//博文删除
router.get('/links/delete', (req, res) => {
    let id = req.query.id || ''
    Links.remove({_id: id}).then(function () {
        res.render('admin/success', {
            userInfo: req.userInfo,
            message: '删除成功',
            url: '/admin/links'
        })
    })
});

//资源库首页
router.get('/asset', (req, res) => {
    let page = Number(req.query.page || 1);
    let limit = 2;
    let pages = 0;

    Asset.countDocuments().then(function (count) {
        pages = Math.ceil(count / limit);
        page = Math.min(page, pages);
        page = Math.max(page, 1);
        let skip = (page - 1) * limit;
        Asset.find().limit(limit).skip(skip).then(function (assets) {
            res.render('admin/asset_index', {
                userInfo: req.userInfo,
                assets: assets,
                count: count,
                pages: pages,
                limit: limit,
                page: page
            })
        })
    })
});

//上传资源
router.get('/upload', (req, res) => {
    res.render('admin/asset_add');
});

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/assets/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

let upload = multer({
    storage: storage
});

//保存资源
router.post('/upload', upload.single("upload"), async (req, res, next) => {
    // 保存数据到数据库
    let file = req.file;
    if (typeof file !== 'undefined') {
        let path = file.path;
        let filename = file.filename;
        console.log("path:" + path);
        console.log("filename:" + filename);
        new Asset({
            filename: filename,
            path: path,
        }).save().then(function (rs) {
            res.render('admin/success', {
                userInfo: req.userInfo,
                message: '保存成功',
                url: '/admin/asset'
            })
        })
    }
});

//下载资源
router.get('/asset/download', function (req, res, next) {
    let path = req.query.path;
    let road = config.__rootDir + '/' + path;
    res.download(road);
});

module.exports = router;