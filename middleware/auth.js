const hasSignedIn = async function (req, res, next) {
    try {
        if (req.user) {
            next();
        }
    } catch (e) {
        throw new Error("您还未登录")
    }

}

const roleStudent = async function (req, res, next) {

    if (req.user.role !== "student") {
        throw new Error("请授权给Student角色")
    }
    next();
}

const roleTeacher = async function (req, res, next) {
    if (req.user.role !== "teacher") {
        throw new Error("请授权给Teacher角色")
    }
    next();
}
const roleAdministrator = async function (req, res, next) {
    if (req.user.role !== "administrator") {
        throw new Error("请授权给Administrator角色")
    }
    next();
}

module.exports = {
    hasSignedIn: hasSignedIn,
    roleStudent: roleStudent,
    roleAdministrator: roleAdministrator,
    roleTeacher: roleTeacher
};